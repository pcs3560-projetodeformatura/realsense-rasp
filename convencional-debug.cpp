// License: Apache 2.0. See LICENSE file in root directory.
// Copyright(c) 2015-2017 Intel Corporation. All Rights Reserved.

#include <librealsense2/rs.hpp> // Include RealSense Cross Platform API
#include "example.hpp"          // Include short list of convenience functions for rendering

#include <algorithm> // std::min, std::max

#include "utils.hpp"

#include <wiringPi.h>
#include <softPwm.h>

/*     mpu6050                  start                         */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include "I2Cdev.h"
#include "MPU6050_6Axis_MotionApps20.h"
#include "helper_3dmath.h"
#include "MPU6050.h"

// class default I2C address is 0x68
// specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for SparkFun breakout and InvenSense evaluation board)
// AD0 high = 0x69
MPU6050 mpu;

// uncomment "OUTPUT_READABLE_QUATERNION" if you want to see the actual
// quaternion components in a [w, x, y, z] format (not best for parsing
// on a remote host such as Processing or something though)
//~ #define OUTPUT_READABLE_QUATERNION

// uncomment "OUTPUT_READABLE_EULER" if you want to see Euler angles
// (in degrees) calculated from the quaternions coming from the FIFO.
// Note that Euler angles suffer from gimbal lock (for more info, see
// http://en.wikipedia.org/wiki/Gimbal_lock)
//~ #define OUTPUT_READABLE_EULER

// uncomment "OUTPUT_READABLE_YAWPITCHROLL" if you want to see the yaw/
// pitch/roll angles (in degrees) calculated from the quaternions coming
// from the FIFO. Note this also requires gravity vector calculations.
// Also note that yaw/pitch/roll angles suffer from gimbal lock (for
// more info, see: http://en.wikipedia.org/wiki/Gimbal_lock)
//~ #define OUTPUT_READABLE_YAWPITCHROLL

// uncomment "OUTPUT_READABLE_REALACCEL" if you want to see acceleration
// components with gravity removed. This acceleration reference frame is
// not compensated for orientation, so +X is always +X according to the
// sensor, just without the effects of gravity. If you want acceleration
// compensated for orientation, us OUTPUT_READABLE_WORLDACCEL instead.
//#define OUTPUT_READABLE_REALACCEL

// uncomment "OUTPUT_READABLE_WORLDACCEL" if you want to see acceleration
// components with gravity removed and adjusted for the world frame of
// reference (yaw is relative to initial orientation, since no magnetometer
// is present in this case). Could be quite handy in some cases.
//#define OUTPUT_READABLE_WORLDACCEL

// uncomment "OUTPUT_TEAPOT" if you want output that matches the
// format used for the InvenSense teapot demo
//#define OUTPUT_TEAPOT

// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternion container
Quaternion* q_pointer;
VectorInt16 aa;         // [x, y, z]            accel sensor measurements
VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity;    // [x, y, z]            gravity vector
float euler[3];         // [psi, theta, phi]    Euler angle container
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector
float cos_xy, sin_xy, cos_yz, sin_yz;


/*     mpu6050                  end                         */

void mpu6050_setup() {
    // initialize device
    // printf("Initializing I2C devices...\n");
    mpu.initialize();

    // verify connection
    // printf("Testing device connections...\n");
    // printf(mpu.testConnection() ? "MPU6050 connection successful\n" : "MPU6050 connection failed\n");

    // load and configure the DMP
    // printf("Initializing DMP...\n");
    devStatus = mpu.dmpInitialize();
    
    // make sure it worked (returns 0 if so)
    if (devStatus == 0) {
        // turn on the DMP, now that it's ready
        printf("Enabling DMP...\n");
        mpu.setDMPEnabled(true);

        // enable Arduino interrupt detection
        //Serial.println(F("Enabling interrupt detection (Arduino external interrupt 0)..."));
        //attachInterrupt(0, dmpDataReady, RISING);
        mpuIntStatus = mpu.getIntStatus();

        // set our DMP Ready flag so the main loop() function knows it's okay to use it
        // printf("DMP ready!\n");
        dmpReady = true;

        // get expected DMP packet size for later comparison
        packetSize = mpu.dmpGetFIFOPacketSize();
    } else {
        // ERROR!
        // 1 = initial memory load failed
        // 2 = DMP configuration updates failed
        // (if it's going to break, usually the code will be 1)
        printf("DMP Initialization failed (code %d)\n", devStatus);
    }
}

void pin_config() {
    wiringPiSetup();
    pinMode(0, OUTPUT);
    pinMode(1, OUTPUT);
    pinMode(2, OUTPUT);
    pinMode(3, OUTPUT);
    pinMode(4, OUTPUT);
    pinMode(5, OUTPUT);
    pinMode(6, OUTPUT);
    pinMode(7, OUTPUT);
    pinMode(10, OUTPUT);

    softPwmCreate(0, 0, 2);
    softPwmCreate(1, 0, 2);
    softPwmCreate(2, 0, 2);
    softPwmCreate(3, 0, 2);
    softPwmCreate(4, 0, 2);
    softPwmCreate(5, 0, 2);
    softPwmCreate(6, 0, 2);
    softPwmCreate(7, 0, 2);
    softPwmCreate(10, 0, 2);
}


void draw_pointcloud2(float width, float height, glfw_state &app_state, rs2::points &points, float cos_xy, float sin_xy, float cos_yz, float sin_yz)
{	
    if (!points)
        return;
	float cells[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0};
	float cells_ground[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0};
	float cells_middle[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0};
	float cells_high[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0};

    // OpenGL commands that prep screen for the pointcloud
    glPopMatrix();
    glPushAttrib(GL_ALL_ATTRIB_BITS);

    glClearColor(153.f / 255, 153.f / 255, 153.f / 255, 1);
    glClear(GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    gluPerspective(60, width / height, 0.01f, 10.0f);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    gluLookAt(0, 0, 0, 0, 0, 1, 0, -1, 0);

    glTranslatef(0, 0, +0.5f + app_state.offset_y * 0.05f);
    glRotated(app_state.pitch, 1, 0, 0);
    glRotated(app_state.yaw, 0, 1, 0);
    glTranslatef(0, 0, -0.5f);

    glPointSize(width / 640);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, app_state.tex.get_gl_handle());
    float tex_border_color[] = {0.8f, 0.8f, 0.8f, 0.8f};
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, tex_border_color);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, 0x812F); // GL_CLAMP_TO_EDGE
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, 0x812F); // GL_CLAMP_TO_EDGE
    
    
    
    
    
    
	float relativeY_x = 0;
    float relativeY_y = 1;
    float relativeY_z = 0;
    float planeD = 1.f;
    
    
	relativeY_x = -sin_xy;
    relativeY_y = -cos_xy * cos_yz;
    relativeY_z = -sin_yz;
    float magnitude = sqrt(relativeY_x*relativeY_x + relativeY_y*relativeY_y + relativeY_z*relativeY_z);
    relativeY_x /= magnitude;
    relativeY_y /= magnitude;
    relativeY_z /= magnitude;
    
    
    


    float side_div_2 = 1.5f;
    float heightY = planeD;
    float z_translation = 2.f;
    // float z_translation = 0;
    float intermediatePoint = 0.5f;

    // float A[3] = {-side_div_2, height, -side_div_2};
    // float B[3] = {side_div_2, height, -side_div_2};
    // float C[3] = {side_div_2, height, side_div_2};
    // float D[3] = {-side_div_2, height, side_div_2};


    //A
    float aux_x = -side_div_2; float aux_y = heightY; float aux_z = -side_div_2 + z_translation;
	//~ VectorFloat Avector(-side_div_2, height, -side_div_2 + z_translation);
	//~ Avector = Avector.getRotated(q);
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float A[3] = {aux_x, aux_y, aux_z};
    //~ float A[3] = {Avector.x, Avector.y, Avector.z};

    //A1
    aux_x = -intermediatePoint; aux_y = heightY; aux_z = -side_div_2 + z_translation;
	//~ Avector = {-intermediatePoint, height, -side_div_2 + z_translation};
	//~ Avector = Avector.getRotated(q);
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float A1[3] = {aux_x, aux_y, aux_z};

    //A2
    aux_x = intermediatePoint; aux_y = heightY; aux_z = -side_div_2 + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float A2[3] = {aux_x, aux_y, aux_z};

    //B
    aux_x = side_div_2; aux_y = heightY; aux_z = -side_div_2 + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float B[3] = {aux_x, aux_y, aux_z};

    //B1
    aux_x = side_div_2; aux_y = heightY; aux_z = -intermediatePoint + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float B1[3] = {aux_x, aux_y, aux_z};

    //B2
    aux_x = side_div_2; aux_y = heightY; aux_z = intermediatePoint + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float B2[3] = {aux_x, aux_y, aux_z};

    //C
    aux_x = side_div_2; aux_y = heightY; aux_z = side_div_2 + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float C[3] = {aux_x, aux_y, aux_z};

    //C1
    aux_x = intermediatePoint; aux_y = heightY; aux_z = side_div_2 + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float C1[3] = {aux_x, aux_y, aux_z};

    //C2
    aux_x = -intermediatePoint; aux_y = heightY; aux_z = side_div_2 + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float C2[3] = {aux_x, aux_y, aux_z};

    //D
    aux_x = -side_div_2; aux_y = heightY; aux_z = side_div_2 + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float D[3] = {aux_x, aux_y, aux_z};

    //D1
    aux_x = -side_div_2; aux_y = heightY; aux_z = intermediatePoint + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float D1[3] = {aux_x, aux_y, aux_z};

    //D2
    aux_x = -side_div_2; aux_y = heightY; aux_z = -intermediatePoint + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float D2[3] = {aux_x, aux_y, aux_z};

    glClear(GL_DEPTH_BUFFER_BIT);
    

    glBegin(GL_POINTS);

    /* this segment actually prints the pointcloud */
    auto vertices = points.get_vertices();              // get vertices
    auto tex_coords = points.get_texture_coordinates(); // and texture coordinates
    for (int i = 0; i < points.size(); i++)
    {
        if (vertices[i].z && vertices[i].z < 5.f && vertices[i].y < planeD)
        {
            // upload the point and texture coordinates only for points we have depth data for
            // if (vertices[i].y < 0.5f)
            // {
            //     glColor3ub(0,   // R
            //                255, // G
            //                0);  // B
            // }
            // else {
                //~ glColor3ub(0, // R
                           //~ 255,   // G
                           //~ 0);  // B
            // }
            //~ std::cout << "oi" << std::endl;
            float x_draw = vertices[i].x;
            float y_draw = vertices[i].y;
            float z_draw = vertices[i].z;
            rotatePoint(x_draw, y_draw, z_draw, cos_xy, sin_xy, cos_yz, sin_yz);
            float originalY = y_draw;
            project_point(x_draw, y_draw, z_draw, relativeY_x, relativeY_y, relativeY_z, planeD);
            //~ rotatePoint(x_draw, y_draw, z_draw, cos_xy, sin_xy, cos_yz, sin_yz);
            // glVertex3fv(vertices[i]);

            // glClearColor(153.f / 255, 153.f / 255, 153.f / 255, 1);
            // glClear(GL_DEPTH_BUFFER_BIT);
            // std::cout << vertices[i].z << std::endl;
            
            //cell 0
            if ( x_draw >= A[0] && x_draw < A1[0] && z_draw >= A[2] && z_draw < D2[2] ) {
                cells[0]++;
				if (originalY < 1.f &&  originalY >= 0.5f) {
					cells_ground[0]++;
				}
				else if (originalY < 0.5f &&  originalY >= -.5f) {
					cells_middle[0]++;
				}
				else if (originalY < -.5f) {
					cells_high[0]++;
				}
			}
            //cell 1
            else if ( x_draw >= A1[0] && x_draw < A2[0] && z_draw >= A[2] && z_draw < D2[2] ){
                cells[1]++; 
                if (originalY < 1.f &&  originalY >= 0.5f) {
					cells_ground[1]++;
				}
				else if (originalY < 0.5f &&  originalY >= -.5f) {
					cells_middle[1]++;
				}
				else if (originalY < -.5f) {
					cells_high[1]++;
				}
			}
            //cell 2
            else if ( x_draw >= A2[0] && x_draw < B[0] && z_draw >= A[2] && z_draw < D2[2] ) {
                cells[2]++;
                if (originalY < 1.f &&  originalY >= 0.5f) {
					cells_ground[2]++;
				}
				else if (originalY < 0.5f &&  originalY >= -.5f) {
					cells_middle[2]++;
				}
				else if (originalY < -.5f) {
					cells_high[2]++;
				}
			}
            //cell 3
            else if ( x_draw >= A[0] && x_draw < A1[0] && z_draw >= D2[2] && z_draw < D1[2] ) {
                cells[3]++;
                if (originalY < 1.f &&  originalY >= 0.5f) {
					cells_ground[3]++;
				}
				else if (originalY < 0.5f &&  originalY >= -.5f) {
					cells_middle[3]++;
				}
				else if (originalY < -.5f) {
					cells_high[3]++;
				}
			}
            //cell 4
            else if ( x_draw >= A1[0] && x_draw < A2[0] && z_draw >= D2[2] && z_draw < D1[2] ) {
                cells[4]++;
                if (originalY < 1.f &&  originalY >= 0.5f) {
					cells_ground[4]++;
				}
				else if (originalY < 0.5f &&  originalY >= -.5f) {
					cells_middle[4]++;
				}
				else if (originalY < -.5f) {
					cells_high[4]++;
				}
			}
            //cell 5
            else if ( x_draw >= A2[0] && x_draw < B[0] && z_draw >= D2[2] && z_draw < D1[2] ) {
                cells[5]++;
                if (originalY < 1.f &&  originalY >= 0.5f) {
					cells_ground[5]++;
				}
				else if (originalY < 0.5f &&  originalY >= -.5f) {
					cells_middle[5]++;
				}
				else if (originalY < -.5f) {
					cells_high[5]++;
				}
			}
            //cell 6
            else if ( x_draw >= A[0] && x_draw < A1[0] && z_draw >= D1[2] && z_draw < D[2] ) {
                cells[6]++;
                if (originalY < 1.f &&  originalY >= 0.5f) {
					cells_ground[6]++;
				}
				else if (originalY < 0.5f &&  originalY >= -.5f) {
					cells_middle[6]++;
				}
				else if (originalY < -.5f) {
					cells_high[6]++;
				}
			}
            //cell 7
            else if ( x_draw >= A1[0] && x_draw < A2[0] && z_draw >= D1[2] && z_draw < D[2] ) {
                cells[7]++;
                if (originalY < 1.f &&  originalY >= 0.5f) {
					cells_ground[7]++;
				}
				else if (originalY < 0.5f &&  originalY >= -.5f) {
					cells_middle[7]++;
				}
				else if (originalY < -.5f) {
					cells_high[7]++;
				}
			}
            //cell 8
            else if ( x_draw >= A2[0] && x_draw < B[0] && z_draw >= D1[2] && z_draw < D[2] ) {
                cells[8]++;
                if (originalY < 1.f &&  originalY >= 0.5f) {
					cells_ground[8]++;
				}
				else if (originalY < 0.5f &&  originalY >= -.5f) {
					cells_middle[8]++;
				}
				else if (originalY < -.5f) {
					cells_high[8]++;
				}
			}
			if (originalY < 1.f &&  originalY >= 0.5f) {
				glColor3ub(0,255,0);
			}
			else if (originalY < 0.5f &&  originalY >= -.5f) {
				glColor3ub(255,255,0);
			}
			else if (originalY < -.5f) {
				glColor3ub(255,0,0);
			}
			
			glVertex3f(x_draw, y_draw, z_draw);
            glTexCoord2fv(tex_coords[i]);
        }
    }
    glEnd();

    draw_axes(relativeY_x, relativeY_y, relativeY_z);
    
    
    for (int i = 0; i < 8; i++) {
        //~ std::cout << "cells[" + std::to_string(i) + "]: " << cells[i] << std::endl;
        if (cells[i] > 10000)
			softPwmWrite(i, 2);
		else 
			softPwmWrite(i, 0);
    }
    if (cells[8] > 10000)
		softPwmWrite(10, 2);
	else 
		softPwmWrite(10, 0);
    //~ glClear(GL_DEPTH_BUFFER_BIT);

    //~ draw_plane_reference(cos_xy, sin_xy, cos_yz, sin_yz, relativeY_x, relativeY_y, relativeY_z, 0.5f);
	glClear(GL_DEPTH_BUFFER_BIT);
        
    glLineWidth(3.0f);
    glBegin(GL_LINES);
	//Draw plane
    // AB
    glColor3ub(255, 255, 255); //     
    glVertex3f(A[0], A[1], A[2]);
    glVertex3f(B[0], B[1], B[2]);

    // BC
    glColor3ub(255, 255, 255); //     
    glVertex3f(B[0], B[1], B[2]);
    glVertex3f(C[0], C[1], C[2]);

    // CD
    glColor3ub(255, 255, 255); //     
    glVertex3f(C[0], C[1], C[2]);
    glVertex3f(D[0], D[1], D[2]);

    // DA
    glColor3ub(255, 255, 255); //     
    glVertex3f(D[0], D[1], D[2]);
    glVertex3f(A[0], A[1], A[2]);

    // A1C2
    glColor3ub(255, 255, 255); //     
    glVertex3f(A1[0], A1[1], A1[2]);
    glVertex3f(C2[0], C2[1], C2[2]);

    // A2C1
    glColor3ub(255, 255, 255); //     
    glVertex3f(A2[0], A2[1], A2[2]);
    glVertex3f(C1[0], C1[1], C1[2]);

    // B1D2
    glColor3ub(255, 255, 255); //     
    glVertex3f(B1[0], B1[1], B1[2]);
    glVertex3f(D2[0], D2[1], D2[2]);

    // B2D1
    glColor3ub(255, 255, 255); //     
    glVertex3f(B2[0], B2[1], B2[2]);
    glVertex3f(D1[0], D1[1], D1[2]);

    glClearColor(153.f / 255, 153.f / 255, 153.f / 255, 1);
    glClear(GL_DEPTH_BUFFER_BIT);

    glEnd();

    // OpenGL cleanup
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glPopAttrib();
    glPushMatrix();
}

// Helper functions
void register_glfw_callbacks(window &app, glfw_state &app_state);

int main(int argc, char *argv[]) try
{
	mpu6050_setup();
	pin_config();
	q_pointer = &q;
    // Create a simple OpenGL window for rendering:
    window app(1280, 720, "RealSense Pointcloud Example");
    // Construct an object to manage view state
    glfw_state app_state;
    // register callbacks to allow manipulation of the pointcloud
    register_glfw_callbacks(app, app_state);

    // Declare pointcloud object, for calculating pointclouds and texture mappings
    rs2::pointcloud pc;
    // We want the points object to be persistent so we can display the last cloud when a frame drops
    rs2::points points;

    // Declare RealSense pipeline, encapsulating the actual device and sensors
    rs2::pipeline pipe;
    // Start streaming with default recommended configuration
    pipe.start();
    

    
	cos_xy=1;  sin_xy=0; cos_yz=1; sin_yz=0;
    while (app) // Application still alive?
    {
		
		if (!dmpReady) continue;
		fifoCount = mpu.getFIFOCount();
		
		if (fifoCount == 1024) {
        // reset so we can continue cleanly
			mpu.resetFIFO();
			// printf("FIFO overflow!\n");

		// otherwise, check for DMP data ready interrupt (this should happen frequently)
		} else if (fifoCount >= 42) {
			mpu.getFIFOBytes(fifoBuffer, packetSize);
            mpu.dmpGetQuaternion(&q, fifoBuffer);
            mpu.dmpGetGravity(&gravity, &q);
            mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
            cos_xy = cos(ypr[2]);  sin_xy = sin(ypr[2]); cos_yz = cos(ypr[1]);  sin_yz = sin(ypr[1]);
            // printf("ypr  %7.2f %7.2f %7.2f    ", ypr[0] * 180/M_PI, ypr[1] * 180/M_PI, ypr[2] * 180/M_PI);
            mpu.resetFIFO();
		}
		mpu.resetFIFO();
		
        // Wait for the next set of frames from the camera
        auto frames = pipe.wait_for_frames();

        auto depth = frames.get_depth_frame();

        // Generate the pointcloud and texture mappings
        points = pc.calculate(depth);

        auto color = frames.get_color_frame();

        // For cameras that don't have RGB sensor, we'll map the pointcloud to infrared instead of color
        if (!color)
            color = frames.get_infrared_frame();

        // Tell pointcloud object to map to this color frame
        pc.map_to(color);

        // Upload the color frame to OpenGL
        app_state.tex.upload(color);

        // Draw the pointcloud
        draw_pointcloud2(app.width(), app.height(), app_state, points, cos_xy, sin_xy, cos_yz, sin_yz);
    }
    
   for (int i = 0; i < 8; i++) {

		softPwmWrite(i, 0);

    }
    softPwmWrite(10, 0);

    return EXIT_SUCCESS;
}
catch (const rs2::error &e)
{
    std::cerr << "RealSense error calling " << e.get_failed_function() << "(" << e.get_failed_args() << "):\n    " << e.what() << std::endl;
    return EXIT_FAILURE;
}
catch (const std::exception &e)
{
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
}
