/*
 * This file is part of the OpenKinect Project. http://www.openkinect.org
 *
 * Copyright (c) 2010 individual OpenKinect contributors. See the CONTRIB file
 * for details.
 *
 * This code is licensed to you under the terms of the Apache License, version
 * 2.0, or, at your option, the terms of the GNU General Public License,
 * version 2.0. See the APACHE20 and GPL2 files for the text of the licenses,
 * or the following URLs:
 * http://www.apache.org/licenses/LICENSE-2.0
 * http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * If you redistribute this file in source form, modified or unmodified, you
 * may:
 *   1) Leave this header intact and distribute it under the same terms,
 *      accompanying it with the APACHE20 and GPL20 files, or
 *   2) Delete the Apache 2.0 clause and accompany it with the GPL2 file, or
 *   3) Delete the GPL v2 clause and accompany it with the APACHE20 file
 * In all cases you must keep the copyright notice intact and include a copy
 * of the CONTRIB file.
 *
 * Binary distributions must follow the binary distribution requirements of
 * either License.
 */

#include <cstdlib>
#include <iostream>
#include <vector>
#include <pthread.h>
#include <libfreenect.hpp>
#include <math.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <softPwm.h>

#if defined(__APPLE__)
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#define PI 3.14159265

class Mutex
{
  public:
    Mutex()
    {
        pthread_mutex_init(&m_mutex, NULL);
    }

    void lock()
    {
        pthread_mutex_lock(&m_mutex);
    }

    void unlock()
    {
        pthread_mutex_unlock(&m_mutex);
    }

    class ScopedLock
    {
      public:
        ScopedLock(Mutex &mutex) : _mutex(mutex)
        {
            _mutex.lock();
        }

        ~ScopedLock()
        {
            _mutex.unlock();
        }

      private:
        Mutex &_mutex;
    };

  private:
    pthread_mutex_t m_mutex;
};

class MyFreenectDevice : public Freenect::FreenectDevice
{
  public:
    MyFreenectDevice(freenect_context *_ctx, int _index)
        : Freenect::FreenectDevice(_ctx, _index),
          m_buffer_video(freenect_find_video_mode(FREENECT_RESOLUTION_MEDIUM, FREENECT_VIDEO_RGB).bytes),
          m_buffer_depth(freenect_find_depth_mode(FREENECT_RESOLUTION_MEDIUM, FREENECT_DEPTH_REGISTERED).bytes / 2),
          m_new_rgb_frame(false), m_new_depth_frame(false)
    {
        setDepthFormat(FREENECT_DEPTH_REGISTERED);
    }

    // Do not call directly, even in child
    void VideoCallback(void *_rgb, uint32_t timestamp)
    {
        Mutex::ScopedLock lock(m_rgb_mutex);
        uint8_t *rgb = static_cast<uint8_t *>(_rgb);
        copy(rgb, rgb + getVideoBufferSize(), m_buffer_video.begin());
        m_new_rgb_frame = true;
    }

    // Do not call directly, even in child
    void DepthCallback(void *_depth, uint32_t timestamp)
    {
        Mutex::ScopedLock lock(m_depth_mutex);
        uint16_t *depth = static_cast<uint16_t *>(_depth);
        copy(depth, depth + getDepthBufferSize() / 2, m_buffer_depth.begin());
        m_new_depth_frame = true;
    }

    bool getRGB(std::vector<uint8_t> &buffer)
    {
        Mutex::ScopedLock lock(m_rgb_mutex);

        if (!m_new_rgb_frame)
            return false;

        buffer.swap(m_buffer_video);
        m_new_rgb_frame = false;

        return true;
    }

    bool getDepth(std::vector<uint16_t> &buffer)
    {
        Mutex::ScopedLock lock(m_depth_mutex);

        if (!m_new_depth_frame)
            return false;

        buffer.swap(m_buffer_depth);
        m_new_depth_frame = false;

        return true;
    }

  private:
    Mutex m_rgb_mutex;
    Mutex m_depth_mutex;
    std::vector<uint8_t> m_buffer_video;
    std::vector<uint16_t> m_buffer_depth;
    bool m_new_rgb_frame;
    bool m_new_depth_frame;
};

Freenect::Freenect freenect;
MyFreenectDevice *device;

Freenect::FreenectTiltState *tilt_state_pointer;



int window(0);                // Glut window identifier
int mx = -1, my = -1;         // Prevous mouse coordinates
float anglex = 0, angley = 0; // Panning angles
float zoom = 1;               // Zoom factor
bool color = true;            // Flag to indicate to use of color in the cloud

double x, y, z;
float gravity, yz_inclination, xy_inclination, sin_xy, cos_xy, sin_yz, cos_yz, x_draw, y_draw, z_draw;
float relativeY_x, relativeY_y, relativeY_z;

void rotatePoint(float& x, float& y, float& z, float cos_xy, float sin_xy, float cos_yz, float sin_yz) {

    float aux_x = x; float aux_y = y; float aux_z = z;

    x = aux_x*cos_xy - aux_y*sin_xy;
        
    y = - aux_x*sin_xy + aux_y*cos_xy;

    z = aux_y*sin_yz + aux_z*cos_yz;

    y = y*cos_yz + z*sin_yz;
    
}

void project_point(float& x, float& y, float& z, float relativeY_x, float relativeY_y, float relativeY_z, float planeD) {
    float v[3] = {x - 0, y - planeD, z - 0};

    float dist = v[0]*relativeY_x + v[1]*relativeY_y + v[2]*relativeY_z;
    x = x - dist*relativeY_x; y = y -dist*relativeY_y; z = z - dist*relativeY_z;
}

void pin_config() {
    wiringPiSetup();
    pinMode(0, OUTPUT);
    pinMode(1, OUTPUT);
    pinMode(2, OUTPUT);
    pinMode(3, OUTPUT);
    pinMode(4, OUTPUT);
    pinMode(5, OUTPUT);
    pinMode(6, OUTPUT);
    pinMode(7, OUTPUT);
    pinMode(8, OUTPUT);

    softPwmCreate(0, 0, 2);
    softPwmCreate(1, 0, 2);
    softPwmCreate(2, 0, 2);
    softPwmCreate(3, 0, 2);
    softPwmCreate(4, 0, 2);
    softPwmCreate(5, 0, 2);
    softPwmCreate(6, 0, 2);
    softPwmCreate(7, 0, 2);
    softPwmCreate(8, 0, 2);
}
            // float v[3] = {x_draw - 0, y_draw, z_draw - 0};
            
            // float dist = v[0]*relativeY_x + v[1]*relativeY_y + v[2]*relativeY_z;
            // float projected_point[3] = { x_draw - dist*relativeY_x, y_draw - dist*relativeY_y, z_draw - dist*relativeY_z };


// void displayText( float x, float y, int r, int g, int b, const char *string ) {
// 	int j = strlen( string );
 
// 	glColor3f( r, g, b );
// 	glRasterPos2f( x, y );
// 	for( int i = 0; i < j; i++ ) {
// 		glutBitmapCharacter( GLUT_BITMAP_TIMES_ROMAN_24, string[i] );
// 	}
// }


void DrawGLScene()
{
    static std::vector<uint8_t> rgb(640 * 480 * 3);
    static std::vector<uint16_t> depth(640 * 480);

    // glut.glutStrokeString(GLUT.STROKE_MONO_ROMAN, "ID: ");

    device->updateState();
    device->getRGB(rgb);
    device->getDepth(depth);


    tilt_state_pointer->getAccelerometers(&x, &y, &z);
    gravity = sqrt(x * x + y * y + z * z);
    yz_inclination = asin(z / gravity) * 180 / PI;

    // xy_inclination = asin(x/gravity) * 180/ PI;
    xy_inclination = asin(x / gravity);
    // std::cout << gravity;
    sin_xy = x / gravity;
    cos_xy = y / gravity;

    sin_yz = z / gravity;
    cos_yz = y / gravity;
    gravity = sqrt(x*x + y*y + z*z);
    yz_inclination = asin(z/gravity) * 180/ PI;


    relativeY_x = -sin_xy;
    relativeY_y = -cos_xy * cos_yz;
    relativeY_z = sin_yz;
    float magnitude = sqrt(relativeY_x*relativeY_x + relativeY_y*relativeY_y + relativeY_z*relativeY_z);
    relativeY_x /= magnitude;
    relativeY_y /= magnitude;
    relativeY_z /= magnitude;
    // glVertex3f(-100.f * sin_xy, -100.f * cos_xy * cos_yz, 100.f * sin_yz);
    

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Draw the world coordinate frame
    glLineWidth(3.0f);
    glBegin(GL_LINES);

    glColor3ub(255, 0, 0); // X-axis
    glVertex3f(0, 0, 0);
    glVertex3f(50, 0, 0);

    glColor3ub(0, 255, 0); // Y-axis
    glVertex3f(0, 0, 0);
    glVertex3f(0, 50, 0);

    glColor3ub(0, 0, 255); // Z-axis
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, 50);

    glColor3ub(0, 0, 255); // Y-Axis corrected
    glVertex3f(0, 0, 0);
    glVertex3f(100.f * relativeY_x, 100.f * relativeY_y, 100.f * relativeY_z);

    // plane

    float side_div_2 = 1500;
    float height = 1000;
    float z_translation = 2500;
    // float z_translation = 0;
    float intermediatePoint = 500;

    // float A[3] = {-side_div_2, height, -side_div_2};
    // float B[3] = {side_div_2, height, -side_div_2};
    // float C[3] = {side_div_2, height, side_div_2};
    // float D[3] = {-side_div_2, height, side_div_2};

    //A
    float aux_x = -side_div_2; float aux_y = height; float aux_z = -side_div_2 + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, 1000);
    float A[3] = {aux_x, aux_y, aux_z};

    //A1
    aux_x = -intermediatePoint; aux_y = height; aux_z = -side_div_2 + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, 1000);
    float A1[3] = {aux_x, aux_y, aux_z};

    //A2
    aux_x = intermediatePoint; aux_y = height; aux_z = -side_div_2 + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, 1000);
    float A2[3] = {aux_x, aux_y, aux_z};

    //B
    aux_x = side_div_2; aux_y = height; aux_z = -side_div_2 + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, 1000);
    float B[3] = {aux_x, aux_y, aux_z};

    //B1
    aux_x = side_div_2; aux_y = height; aux_z = -intermediatePoint + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, 1000);
    float B1[3] = {aux_x, aux_y, aux_z};

    //B2
    aux_x = side_div_2; aux_y = height; aux_z = intermediatePoint + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, 1000);
    float B2[3] = {aux_x, aux_y, aux_z};

    //C
    aux_x = side_div_2; aux_y = height; aux_z = side_div_2 + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, 1000);
    float C[3] = {aux_x, aux_y, aux_z};

    //C1
    aux_x = intermediatePoint; aux_y = height; aux_z = side_div_2 + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, 1000);
    float C1[3] = {aux_x, aux_y, aux_z};

    //C2
    aux_x = -intermediatePoint; aux_y = height; aux_z = side_div_2 + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, 1000);
    float C2[3] = {aux_x, aux_y, aux_z};

    //D
    aux_x = -side_div_2; aux_y = height; aux_z = side_div_2 + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, 1000);
    float D[3] = {aux_x, aux_y, aux_z};

    //D1
    aux_x = -side_div_2; aux_y = height; aux_z = intermediatePoint + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, 1000);
    float D1[3] = {aux_x, aux_y, aux_z};

    //D2
    aux_x = -side_div_2; aux_y = height; aux_z = -intermediatePoint + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, 1000);
    float D2[3] = {aux_x, aux_y, aux_z};

    //Draw plane
    // AB
    glColor3ub(255, 255, 255); //     
    glVertex3f(A[0], A[1], A[2]);
    glVertex3f(B[0], B[1], B[2]);

    // BC
    glColor3ub(255, 255, 255); //     
    glVertex3f(B[0], B[1], B[2]);
    glVertex3f(C[0], C[1], C[2]);

    // CD
    glColor3ub(255, 255, 255); //     
    glVertex3f(C[0], C[1], C[2]);
    glVertex3f(D[0], D[1], D[2]);

    // DA
    glColor3ub(255, 255, 255); //     
    glVertex3f(D[0], D[1], D[2]);
    glVertex3f(A[0], A[1], A[2]);

    // A1C2
    glColor3ub(255, 255, 255); //     
    glVertex3f(A1[0], A1[1], A1[2]);
    glVertex3f(C2[0], C2[1], C2[2]);

    // A2C1
    glColor3ub(255, 255, 255); //     
    glVertex3f(A2[0], A2[1], A2[2]);
    glVertex3f(C1[0], C1[1], C1[2]);

    // B1D2
    glColor3ub(255, 255, 255); //     
    glVertex3f(B1[0], B1[1], B1[2]);
    glVertex3f(D2[0], D2[1], D2[2]);

    // B2D1
    glColor3ub(255, 255, 255); //     
    glVertex3f(B2[0], B2[1], B2[2]);
    glVertex3f(D1[0], D1[1], D1[2]);

    glEnd();


    float cells[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};

    glPointSize(1.0f);

    glBegin(GL_POINTS);

    if (!color)
        glColor3ub(255, 255, 255);
    for (int i = 0; i < 480 * 640; ++i)
    {
        // if (color)
        //     glColor3ub(rgb[3 * i + 0],  // R
        //                rgb[3 * i + 1],  // G
        //                rgb[3 * i + 2]); // B

        if (color)
          glColor3ub(0,  // R
                    255,  // G
                     0); // B

        float f = 595.f;
        x_draw = (i % 640 - (640 - 1) / 2.f) * depth[i] / f;
        y_draw = (i / 640 - (480 - 1) / 2.f) * depth[i] / f;
        z_draw =  depth[i];
        // Convert from image plane coordinates to world coordinates
        // if (x_draw < 0.f)
        if ( x_draw*relativeY_x + y_draw*relativeY_y + z_draw*relativeY_z > -1000  ) {

            // float v[3] = {x_draw - 0, y_draw - 1000, z_draw - 0};
            // float v[3] = {x_draw - 0, y_draw, z_draw - 0};
            
            // float dist = v[0]*relativeY_x + v[1]*relativeY_y + v[2]*relativeY_z;
            // float projected_point[3] = { x_draw - dist*relativeY_x, y_draw - dist*relativeY_y, z_draw - dist*relativeY_z };

            project_point(x_draw, y_draw, z_draw, relativeY_x, relativeY_y, relativeY_z, 1000);
            glVertex3f(x_draw, // X = (x - cx) * d / fx
                   y_draw, // Y = (y - cy) * d / fy
                   z_draw);                                  // Z = d
            // glVertex3f(projected_point[0], // X = (x - cx) * d / fx
            //        projected_point[1], // Y = (y - cy) * d / fy
            //        projected_point[2]);  

            //cell 0
            if ( x_draw >= A[0] && x_draw < A1[0] && z_draw >= A[2] && z_draw < D2[2] )
                cells[0]++;
            //cell 1
            else if ( x_draw >= A1[0] && x_draw < A2[0] && z_draw >= A[2] && z_draw < D2[2] )
                cells[1]++; 
            //cell 2
            else if ( x_draw >= A2[0] && x_draw < B[0] && z_draw >= A[2] && z_draw < D2[2] )
                cells[2]++;
            //cell 3
            else if ( x_draw >= A[0] && x_draw < A1[0] && z_draw >= D2[2] && z_draw < D1[2] )
                cells[3]++;
            //cell 4
            else if ( x_draw >= A1[0] && x_draw < A2[0] && z_draw >= D2[2] && z_draw < D1[2] )
                cells[4]++;
            //cell 5
            else if ( x_draw >= A2[0] && x_draw < B[0] && z_draw >= D2[2] && z_draw < D1[2] )
                cells[5]++;
            //cell 6
            else if ( x_draw >= A[0] && x_draw < A1[0] && z_draw >= D1[2] && z_draw < D[2] )
                cells[6]++;
            //cell 7
            else if ( x_draw >= A1[0] && x_draw < A2[0] && z_draw >= D1[2] && z_draw < D[2] )
                cells[7]++;
            //cell 8
            else if ( x_draw >= A2[0] && x_draw < B[0] && z_draw >= D1[2] && z_draw < D[2] )
                cells[8]++;
        }
    }



    glEnd();

    for (int i = 0; i < 9; i++) {



        std::cout << "cells[" + std::to_string(i) + "]: " << cells[i] << std::endl;
    }
    
 
    // Eigen::Vector3f axis = Eigen::Vector3f(-1.f * sin_xy, -1.f * cos_xy * cos_yz, 1.f * sin_yz); //y axis
    





    // Place the camera
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glScalef(zoom, zoom, 1);
    gluLookAt(-7 * anglex, -7 * angley, -1000.0,
              0.0, 0.0, 2000.0,
              0.0, -1.0, 0.0);

    glutSwapBuffers();
}

void keyPressed(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 'C':
    case 'c':
        color = !color;
        break;

    case 'Q':
    case 'q':
    case 0x1B: // ESC
        glutDestroyWindow(window);
        device->stopDepth();
        device->stopVideo();
        exit(0);
    }
}

void mouseMoved(int x, int y)
{
    if (mx >= 0 && my >= 0)
    {
        anglex += x - mx;
        angley += y - my;
    }

    mx = x;
    my = y;
}

void mouseButtonPressed(int button, int state, int x, int y)
{
    if (state == GLUT_DOWN)
    {
        switch (button)
        {
        case GLUT_LEFT_BUTTON:
            mx = x;
            my = y;
            break;

        case 3:
            zoom *= 1.2f;
            break;

        case 4:
            zoom /= 1.2f;
            break;
        }
    }
    else if (state == GLUT_UP && button == GLUT_LEFT_BUTTON)
    {
        mx = -1;
        my = -1;
    }
}

void resizeGLScene(int width, int height)
{
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(50.0, (float)width / height, 900.0, 11000.0);

    glMatrixMode(GL_MODELVIEW);
}

void idleGLScene()
{
    glutPostRedisplay();
}

void printInfo()
{
    std::cout << "\nAvailable Controls:" << std::endl;
    std::cout << "===================" << std::endl;
    std::cout << "Rotate       :   Mouse Left Button" << std::endl;
    std::cout << "Zoom         :   Mouse Wheel" << std::endl;
    std::cout << "Toggle Color :   C" << std::endl;
}



int main(int argc, char **argv)
{
    pin_config();
    device = &freenect.createDevice<MyFreenectDevice>(0);
    device->startVideo();
    device->startDepth();
        //  tilt_state;
    Freenect::FreenectTiltState tilt_state = device->getState();
    tilt_state_pointer = &tilt_state;

    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(0, 0);

    window = glutCreateWindow("LibFreenect123");
    // glClearColor(0.45f, 0.45f, 0.45f, 0.0f);
    glClearColor(0.f, 0.f, 0.f, 0.0f);
    

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0.0f);

    glMatrixMode(GL_PROJECTION);
    gluPerspective(50.0, 1.0, 900.0, 11000.0);

    glutDisplayFunc(&DrawGLScene);
    glutIdleFunc(&idleGLScene);
    glutReshapeFunc(&resizeGLScene);
    glutKeyboardFunc(&keyPressed);
    glutMotionFunc(&mouseMoved);
    glutMouseFunc(&mouseButtonPressed);

    printInfo();
    std::cout << "Toggle Color :   C" << std::endl;

    glutMainLoop();

    return 0;
}
