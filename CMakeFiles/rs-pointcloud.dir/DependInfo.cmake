# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/pi/librealsense/examples/pointcloud/I2Cdev.cpp" "/home/pi/librealsense/examples/pointcloud/CMakeFiles/rs-pointcloud.dir/I2Cdev.cpp.o"
  "/home/pi/librealsense/examples/pointcloud/MPU6050.cpp" "/home/pi/librealsense/examples/pointcloud/CMakeFiles/rs-pointcloud.dir/MPU6050.cpp.o"
  "/home/pi/librealsense/examples/pointcloud/rs-pointcloud.cpp" "/home/pi/librealsense/examples/pointcloud/CMakeFiles/rs-pointcloud.dir/rs-pointcloud.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BUILD_EASYLOGGINGPP"
  "ELPP_NO_DEFAULT_LOG_FILE"
  "ELPP_THREAD_SAFE"
  "HWM_OVER_XU"
  "RS2_USE_V4L2_BACKEND"
  "UNICODE"
  "USE_SYSTEM_LIBUSB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "realsense2"
  "src"
  "examples/pointcloud/rs-pointcloud"
  "examples/pointcloud/.."
  "include"
  "/usr/include/libusb-1.0"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/pi/librealsense/CMakeFiles/realsense2.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
