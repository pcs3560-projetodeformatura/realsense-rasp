// #include <librealsense2/rs.hpp>
#include "example.hpp"    
#include "helper_3dmath.h"


void project_point(float& x, float& y, float& z, float relativeY_x, float relativeY_y, float relativeY_z, float planeD) {
    float v[3] = {x - 0, y - planeD, z - 0};

    float dist = v[0]*relativeY_x + v[1]*relativeY_y + v[2]*relativeY_z;
    x = x - dist*relativeY_x; y = y -dist*relativeY_y; z = z - dist*relativeY_z;
}

void rotatePoint(float& x, float& y, float& z, float cos_xy, float sin_xy, float cos_yz, float sin_yz) {

    float aux_x = x; float aux_y = y; float aux_z = z;

    x = aux_x*cos_xy - aux_y*sin_xy;
        
    y = - aux_x*sin_xy + aux_y*cos_xy;

    z = aux_y*sin_yz + aux_z*cos_yz;

    y = y*cos_yz - z*sin_yz;
    
}


void draw_axes(float relativeY_x, float relativeY_y, float relativeY_z) {
        // Draw axes
    // glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClear(GL_DEPTH_BUFFER_BIT);
        
    glLineWidth(3.0f);
    glBegin(GL_LINES);

    glColor3ub(255, 0, 0); // X-axis
    glVertex3f(0, 0, 0);
    glVertex3f(1, 0, 0);

    glColor3ub(0, 255, 0); // Y-axis
    glVertex3f(0, 0, 0);
    glVertex3f(0, 1, 0);

    glColor3ub(0, 0, 255); // Z-axis
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, 1);

    glColor3ub(0, 0, 255); // Y-Axis corrected
    glVertex3f(0, 0, 0);
    glVertex3f(100.f * relativeY_x, 100.f * relativeY_y, 100.f * relativeY_z);


    glClearColor(153.f / 255, 153.f / 255, 153.f / 255, 1);
    glClear(GL_DEPTH_BUFFER_BIT);

    glEnd();
}

void draw_plane_reference(float cos_xy, float sin_xy, float cos_yz, float sin_yz, float relativeY_x, float relativeY_y, float relativeY_z, float planeD) {

    glClear(GL_DEPTH_BUFFER_BIT);
        
    glLineWidth(3.0f);
    glBegin(GL_LINES);

    float side_div_2 = 1.5f;
    float height = 1.f;
    float z_translation = 2.5f;
    // float z_translation = 0;
    float intermediatePoint = 0.5f;

    // float A[3] = {-side_div_2, height, -side_div_2};
    // float B[3] = {side_div_2, height, -side_div_2};
    // float C[3] = {side_div_2, height, side_div_2};
    // float D[3] = {-side_div_2, height, side_div_2};


    //A
    float aux_x = -side_div_2; float aux_y = height; float aux_z = -side_div_2 + z_translation;
	//~ VectorFloat Avector(-side_div_2, height, -side_div_2 + z_translation);
	//~ Avector = Avector.getRotated(q);
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float A[3] = {aux_x, aux_y, aux_z};
    //~ float A[3] = {Avector.x, Avector.y, Avector.z};

    //A1
    aux_x = -intermediatePoint; aux_y = height; aux_z = -side_div_2 + z_translation;
	//~ Avector = {-intermediatePoint, height, -side_div_2 + z_translation};
	//~ Avector = Avector.getRotated(q);
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float A1[3] = {aux_x, aux_y, aux_z};

    //A2
    aux_x = intermediatePoint; aux_y = height; aux_z = -side_div_2 + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float A2[3] = {aux_x, aux_y, aux_z};

    //B
    aux_x = side_div_2; aux_y = height; aux_z = -side_div_2 + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float B[3] = {aux_x, aux_y, aux_z};

    //B1
    aux_x = side_div_2; aux_y = height; aux_z = -intermediatePoint + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float B1[3] = {aux_x, aux_y, aux_z};

    //B2
    aux_x = side_div_2; aux_y = height; aux_z = intermediatePoint + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float B2[3] = {aux_x, aux_y, aux_z};

    //C
    aux_x = side_div_2; aux_y = height; aux_z = side_div_2 + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float C[3] = {aux_x, aux_y, aux_z};

    //C1
    aux_x = intermediatePoint; aux_y = height; aux_z = side_div_2 + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float C1[3] = {aux_x, aux_y, aux_z};

    //C2
    aux_x = -intermediatePoint; aux_y = height; aux_z = side_div_2 + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float C2[3] = {aux_x, aux_y, aux_z};

    //D
    aux_x = -side_div_2; aux_y = height; aux_z = side_div_2 + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float D[3] = {aux_x, aux_y, aux_z};

    //D1
    aux_x = -side_div_2; aux_y = height; aux_z = intermediatePoint + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float D1[3] = {aux_x, aux_y, aux_z};

    //D2
    aux_x = -side_div_2; aux_y = height; aux_z = -intermediatePoint + z_translation;
    rotatePoint(aux_x, aux_y, aux_z, cos_xy, sin_xy, cos_yz, sin_yz);
    project_point(aux_x, aux_y, aux_z, relativeY_x, relativeY_y, relativeY_z, planeD);
    float D2[3] = {aux_x, aux_y, aux_z};

    //Draw plane
    // AB
    glColor3ub(255, 255, 255); //     
    glVertex3f(A[0], A[1], A[2]);
    glVertex3f(B[0], B[1], B[2]);

    // BC
    glColor3ub(255, 255, 255); //     
    glVertex3f(B[0], B[1], B[2]);
    glVertex3f(C[0], C[1], C[2]);

    // CD
    glColor3ub(255, 255, 255); //     
    glVertex3f(C[0], C[1], C[2]);
    glVertex3f(D[0], D[1], D[2]);

    // DA
    glColor3ub(255, 255, 255); //     
    glVertex3f(D[0], D[1], D[2]);
    glVertex3f(A[0], A[1], A[2]);

    // A1C2
    glColor3ub(255, 255, 255); //     
    glVertex3f(A1[0], A1[1], A1[2]);
    glVertex3f(C2[0], C2[1], C2[2]);

    // A2C1
    glColor3ub(255, 255, 255); //     
    glVertex3f(A2[0], A2[1], A2[2]);
    glVertex3f(C1[0], C1[1], C1[2]);

    // B1D2
    glColor3ub(255, 255, 255); //     
    glVertex3f(B1[0], B1[1], B1[2]);
    glVertex3f(D2[0], D2[1], D2[2]);

    // B2D1
    glColor3ub(255, 255, 255); //     
    glVertex3f(B2[0], B2[1], B2[2]);
    glVertex3f(D1[0], D1[1], D1[2]);

    glClearColor(153.f / 255, 153.f / 255, 153.f / 255, 1);
    glClear(GL_DEPTH_BUFFER_BIT);

    glEnd();
}
